## Comandos Git, Para projetos dê preferencia ao Eclipse

Mostrar os arquivos que estão para vir no branch (funciona apenas depois do fetch).

`git diff --name-status <branch> origin/<branch>`


Mostrar os arquivos que estão para vir no branch com foco nas linhas modificadas(funciona apenas depois do fetch).

`git diff --minimal <branch> origin/<branch>`
 

Mostrar diferença entre branches

`git diff --name-status <branch1> <branch2>`
 

Mostrar arquivos ignorados

`git status --ignored`
 
Remove o arquivo do stage

`git reset <arquivo>`
 
Para incluir no .gitignore um arquivo que um dia já foi versionado, inclua o caminho no .gitignore, em seguida execute o comando abaixo para o arquivo em questão

`git rm -r --cached <arquivo>`
 
Fazer um branch a partir do master ou branch

`git checkout -b novoBranch antigoBranchOuMaster`

Para 'salvar' seu branch no estado atual, sem precisar fazer commit

`git stash`

Lista o que está em stash

`git stash list`

Volta pro último stash

`git stash apply`

Volta pra um stash específico

`git stash apply <nome do stash>`
