### Situação: fiz um pull e deu merda

Se você não gostou do que aconteceu no pull que você fez, não se desespere.

`git pull` deu merda

`git reflog` um log das atividades do git, vai vir algo assim:
```
abc987  HEAD@{0}: pull
8f3a362 HEAD@{1}: fiz esse commit pois eu sou o cara
```

Ai basta você voltar o seu branch pro estado antes do HEAD@{1} com o comando

`git reset --hard HEAD@{1}`

Pronto!!

![Alt text](http://i.imgur.com/1O5g5If.jpg)